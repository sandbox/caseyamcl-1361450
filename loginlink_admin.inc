<?php

/**
 * @file
 * Loginlink Admin Functions File
 */

//------------------------------------------------------------------------

/**
 * Manage Login Links
 *
 * This is the main administration page for manging login links
 *
 * @return string
 * HTML for the management page
 */
function loginlink_page_manage_login_links() {

  //Get the login links
  $q = db_select('loginlink', 'll')->fields('ll')->condition('processed', NULL)->execute();

  if ($q->rowCount() > 0) {
    $out = '<p>' . t('This page allows you to create and manage one-time login links for users') . '</p>';
    $out .= theme('table', _loginlink_get_table_of_links());

  }
  else {
    $out  = t('There are no login links.');
    $out .= ' ' . l(t("Create Some Loginlinks?"), 'admin/people/loginlinks/create');
    $out = "<p>$out</p>";
  }

  return $out;
}

//------------------------------------------------------------------------

/**
 * Get login links table for display in management page
 *
 * Returns an array of links for management
 *
 * @return array
 * An array that can be sent to theme_table() for processing
 */
function _loginlink_get_table_of_links() {

  $links = loginlink_get_links();

  /* TABLE */
  $table_opts = array();
  $table_opts['header'] = array(
    'User',
    'Created',
    'Expires',
    'Redirect',
    'Message?',
    array('data' => 'Operations', 'colspan' => '2')
  );

  $table_opts['rows'] = array();

  foreach ($links as $r) {

    $table_opts['rows'][] = array(
       user_load($r->uid)->name,
       date('r', $r->created),
       (time() > $r->expiration) ? "<span class='expired'>" . date('r', $r->expiration) . "</span>" : date('r', $r->expiration),
       ($r->login_redirect) ? $r->login_redirect : '<em>' . t('None') . '</em>',
       ( ! empty($r->login_msg)) ?
         "<span title='{$r->login_msg}'>" . truncate_utf8($r->login_msg, 50, FALSE, TRUE) . '</span>' : '<em>' . t('None') . '</em>',
       //@TODO: Uncomment this for next version: l(t('Edit'), 'admin/people/loginlink/' . $r->uid),
       l(t('Revoke'), 'user/' . $r->uid . '/revokeloginlink')
    );
  }

  return $table_opts;
}

//------------------------------------------------------------------------

/**
 * Download login links as a CSV file
 */
function loginlink_download_links() {

  $links = loginlink_get_links();

  $headers = array_map('csv_prep', array('username', 'link', 'expiration', 'new_pass'));

  $rows = array();
  foreach ($links as $r) {

    $rows[] = array_map('csv_prep', array(
      user_load($r->uid)->name,
      url('loginlink/' . $r->login_key, array('absolute' => TRUE)),
      date('r', $r->expiration),
      ($r->regen) ? decrypt($r->regen, array('base64' => TRUE)) : NULL
    ));
  }

  $out = implode(',', $headers) . "\n";
  foreach ($rows as $r) {
    $out .= implode(',', $r) . "\n";
  }

  header("Content-type: text/plain");
  header("Content-disposition: attachment; filename=loginlinks.csv");
  echo $out;
}

//------------------------------------------------------------------------

/**
 * Revoke a loginlink for a user
 *
 * @global object $user
 * The global user, for logging messages
 *
 * @param int $uid
 * The user ID to revoke the login link for
 */
function loginlink_page_revoke_loginlink($uid) {

  //Get the loginlink for that user id
  $q = db_select('loginlink', 'll')->fields('ll')->condition('uid', $uid)->condition('processed', NULL)->execute();

  if ($q->rowCount() > 0) {

    //Delete it
    db_delete('loginlink')->condition('uid', $uid)->condition('processed', NULL)->execute();

    //Watchdog log
    global $user;
    watchdog('loginlink', 'User :adminuser revoked loginlink for @username', array('@adminuser' => $user->name, 'username' => user_load($uid)->name));

    //Message
    drupal_set_message(check_plain(t('Deleted the loginlink for @username', array('@username' => user_load($uid)->name))));

  }
  else {
    drupal_set_message(check_plain(t("Cannot delete.  No such loginlink.")), 'warning');
  }

  //Goto
  drupal_goto('admin/people/loginlinks');
}

//------------------------------------------------------------------------

/**
 * Return a form for managing login links
 *
 * @param int $uid
 * If UID is not null, loads up the form to edit a login link
 *
 * @return array
 * A Drupal form array with the login link form
 */
function loginlink_page_login_link_form($uid = NULL) {

  if ( ! is_null($uid)) {
    return drupal_get_form('loginlink_edit_form', $uid);
  }
  else {
    return drupal_get_form('loginlink_form');
  }

}

//------------------------------------------------------------------------

/**
 * Processor for the loginlink form
 */
function loginlink_form_submit($form, &$form_state) {

  $values =& $form_state['values'];

  //Get the user ids to generate for
  $ids = ($values['user_select_type'] == 'by_role') ? $values['select_by_role']: $values['select_by_user'];
  $uids = _loginlink_get_userlist($ids, $values['user_select_type'], $values['only_use_new_users']);

  //Get UIDs to reset passwords for, if selected
  if ($values['regen_password'] == 'all')
    $reset_ids = $uids;
  elseif ($values['regen_password'] == 'new')
    $reset_ids = _loginlink_get_userlist($ids, $values['user_select_type'], TRUE);
  else
    $reset_ids = FALSE;

  //Criteria array
  $ll_criteria = array();
  $ll_criteria['no_expire_first_login'] = $values['no_expire_new_user'];
  $ll_criteria['login_msg'] = $values['login_display_msg'];
  $ll_criteria['login_redirect'] = $values['login_redirect_path'];

  //Expiration
  if ( ! empty($values['expiration_hours']) && $values['expiration_hours'] > 0) {
    //Convert hours to seconds
    $ll_criteria['expiration'] = $values['expiration_hours'] * 3600;
  }

  //Counts for reporting
  $counts['updated'] = 0;
  $counts['created'] = 0;
  $counts['error'] = 0;
  $counts['emailed'] = 0;
  $counts['passwords'] = 0;

  //Do it!
  foreach ($uids as $uid) {

    //If we are resetting the password as well..
    if ($reset_ids && in_array($uid, $reset_ids)) {

      //If UID is one, do not generate a new password!
      if ($uid == 1) {
        drupal_set_message(check_plain(t('You cannot change the password for the administrative user!')), 'warning');
      }
      else {

        $new_pw = _loginlink_reset_password($uid);

        if ($new_pw) {
          $ll_criteria['regen'] = encrypt($new_pw, array('base64' => TRUE));
          $counts['passwords']++;
        }
      }
    }

    //Do it! -- Add the loginlink
    switch (loginlink_generate($uid, $ll_criteria)) {
      case LOGINLINK_GENERATED: $counts['created']++; break;
      case LOGINLINK_UPDATED: $counts['updated']++; break;
      default: $counts['error']++;
    }

    //If email enabled, send an email
    if ($values['send_email'] == '1') {

      $mail_params = array(
        'subject' => $values['email_subject'],
        'message' => $values['email_body'],
        'account' => user_load($uid)
      );

      $mail_result = drupal_mail('loginlink', 'loginlink_generate', $mail_params['account']->mail, user_preferred_language(user_load($uid)), $mail_params, variable_get('site_mail', NULL), TRUE);

      if ($mail_result)
        $counts['emailed']++;
    }

  }

  //Generate messages
  foreach ($counts as $ct => $num) {

    if ($num > 0) {
      $num = number_format($num, 0);

      switch ($ct) {
        case 'error':
          drupal_set_message(t("There were :num errors", array(':num' => $num)), 'warning');
          break;
        case 'passwords':
          drupal_set_message(t("Reset passwords for :num accounts", array(':num' => $num)));
          break;
        default:
          drupal_set_message(t(':action :num login links', array(':action' => drupal_ucfirst($ct), ':num' => $num)));
      }
    }
  }

  drupal_goto('admin/people/loginlinks');
}

//------------------------------------------------------------------------

/**
 * Validator for the loginlink form
 */
function loginlink_form_validate($form, &$form_state) {

  $values =& $form_state['values'];

  //Ensure there are actually users selected
  $ids = ($values['user_select_type'] == 'by_role') ? $values['select_by_role']: $values['select_by_user'];
  $uids = _loginlink_get_userlist($ids, $values['user_select_type'], $values['only_use_new_users']);
  if (count($uids) < 1) {
    form_set_error('user_select_type', t('Your user selection results in zero users being selected!'));
  }

  //Expiration hours must be a positive integer
  if ( ! empty($values['expiration_hours']) && ((string) (int) $values['expiration_hours'] !== (string) $values['expiration_hours'] OR $values['expiration_hours'] < 0))
    form_set_error('expiration_hours', t('Expiration hours must be a positive integer or zero!'));

  //Login Display Message must be plain!
  if (strcmp($values['login_display_msg'], check_plain($values['login_display_msg'])) != 0)
    form_set_error('login_display_msg', t('The login display message must be plaintext!'));

  //Login Redirect Path - Check valid path
  if ( ! empty($values['login_redirect_path']) && ! drupal_lookup_path('source', $values['login_redirect_path']))
    form_set_error('login_redirect_path', t('The redirect path you specified is not valid!'));

  //If send_email, email subject body required
  //Email body - Typical validation for email messages?
  if ($values['send_email'] == '1') {

    if (empty($values['email_subject']))
      form_set_error('email_subject', t('You must create an email subject!'));

    if (empty($values['email_body']))
      form_set_error('email_body', t('You must create an email body!'));
  }

}

//------------------------------------------------------------------------

/**
 * The loginlink form
 *
 * @param array $form
 * @param array $form_state
 * @param int $uid
 * @return array
 */
function loginlink_form($form, &$form_state) {

  //Header message
  $form['header'] = array(
    '#markup' => '<p>' . t('This page allows you to create a modify login links.') . '</p>'
  );

  $form['user_select'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select Users to create a login link for')
  );

  //Radio choices to allow for (1) by user, (2) by role, (3) all users
  $user_select_options = array(
    'all_users' => t('All Users'),
    'by_user' => t('Select by Username(s)'),
    'by_role' => t('Select by Role')
  );

  $form['user_select']['user_select_type'] = array(
    '#title' => t('Selction Criteria'),
    '#type' => 'radios',
    '#options' => $user_select_options,
    '#default_value' => 'all_users',
    '#required' => TRUE
  );

  //Get the users
  $q = db_select('users', 'u')->fields('u', array('uid', 'name'))->condition('status', '1')->execute();
  $users = array();
  foreach ($q as $u) {
    $users[$u->uid] = $u->name;
  }

  $form['user_select']['select_by_user'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $users,
    '#title' => t('Select User(s)'),
    '#description' => t("Tip: Use CTRL or SHIFT to select multiple users"),
    '#states' => array(
      'visible' => array('input[name=user_select_type]' => array('value' => 'by_user'))
    )
  );

  $form['user_select']['select_by_role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select Role(s)'),
    '#options' => user_roles(TRUE),
    '#states' => array(
      'visible' => array('input[name=user_select_type]' => array('value' => 'by_role'))
    )
  );

  //Checkbox to only send to users that have never logged-in
  $form['user_select']['only_use_new_users'] = array(
    '#type' => 'checkbox',
    '#title' => t('Only generate links for users who have not yet logged in.'),
    '#default_value' => 0
  );

  $form['user_select']['notice'] = array(
    '#markup' => '<p>' . t('Note: Loginlinks will be updated for any users who already have valid links.') . '</p>'
  );

  //Regenerate password options
  $form['regen_password'] = array(
    '#type' => 'radios',
    '#title' => t('Regenerate Password'),
    '#options' => array(
      'none' => 'Do not regenerate passwords',
      'new' => 'Only for users who have not yet logged-in',
      'all' => 'For all users'),
    '#default_value' => 'none',
    '#description' => t('This can be useful if you are exporting links as a CSV and want to include usernames and passwords in the message.')
  );

  //Expiration in Hours
  $form['expiration_hours'] = array(
    '#title' => t('Expiration (in hours)'),
    '#type' => 'textfield',
    '#description' => t('Leave blank or enter zero (0) for no expiration'),
    '#size' => 15,
    '#default_value' => '48'
  );

  //No expiration if new user
  $form['no_expire_new_user'] = array(
    '#title' => t('No expiration if new user'),
    '#description' => t('Check this box to disable login expirations for users who have never logged in before'),
    '#type' => 'checkbox',
    '#default_value' => 0
  );

  //Message to display
  $form['login_display_msg'] = array(
    '#title' => t('Login Message to Display'),
    '#type' => 'textfield',
    '#description' => t('Optionally enter a message to display to users upon logging in.'),
    '#default_value' => t('You have used your one-time login link.  Please visit your account page to change your password.')
  );

  //Redirect path
  $form['login_redirect_path'] = array(
    '#title' => t('Login Redirect'),
    '#description' => t('Optionally enter a path to redirect users to upon logging in.  If blank, default path is used'),
    '#type' => 'textfield'
  );

  //Send an email
  $form['send_email'] = array(
    '#title' => t('Send users an email'),
    '#type' => 'radios',
    '#options' => array('1' => t('Yes (configurable)'), '0' => 'No'),
    '#default_value' => '0'
  );

  $form['email_area'] = array(
    '#type' => 'fieldset',
    '#title' => t('Email Message'),
     '#states' => array(
      'visible' => array('input[name=send_email]' => array('value' => '1'))
    )
  );
 
  $form['email_area']['token_help'] = array(
    '#title' => t('Replacement patterns'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  
  $form['email_area']['token_help']['help'] = array(
    '#value' => theme('token_help', 'user')  
  );
  

  $form['email_area']['email_subject'] = array(
    '#title' => t('Email Subject'),
    '#type' => 'textfield'
  );

  $form['email_area']['email_body'] = array(
    '#title' => t('Email Body'),
    '#type' => 'textarea'
  );

  //Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create login link(s)')
  );

  return $form;
}


//------------------------------------------------------------------------

function loginlink_edit_form($form, &$form_state, $uid) {

  //Get the loginlink
  $ll = db_select('loginlink', 'll')->fields('ll')->execute()->fetch();
 
  if ( ! $ll) {
    drupal_set_message(check_plain(t('Invalid Loginlink Specified')), 'warning');
    drupal_goto('admin/people/loginlink');
  }
  
  //Header message
  $form['header'] = array(
    '#markup' => '<p>' . t('This page allows you to view/modify loginlink for user ' . $uid) . '</p>'
  );
  
  $form['linkinfo'] = array(
    '#type' => 'fieldset',
    '#title' => t('Link Info')
  );
  
  throw new Exception("LEFT OFF HERE LEFT OFF HERE LEFT OFF HERE");
  $linkinfo_table = "Use theme_table to get link key (but not URL), and other info...";
  
  
  $form['linkinfo']['markup'] = array(
    '#markup' => $linkinfo_table
  );

  $form['expiration'] = array(
    '#title' => t('Change Expiration'),
    '#type' => 'textfield',
    '#description' => t('Current expiration is %exp', array('%exp' => 'xx'))
  );
  
  $form['login_display_msg'] = array(
    '#title' => t('Change login Message'),
    '#type' => 'textfield',
    '#default_value' => 'xx'
  );
  
  $form['login_redirect_path'] = array(
    '#title' => t('Change login redirect'),
    '#description' => t('Optionally enter a path to redirect users to upon logging in. If blank, default path is used'),
    '#type' => 'textfield',
    '#default_value' => 'xx'
  );
  
  
  //Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update login link(s)')
  );
 
  
  return $form;
  
}

//------------------------------------------------------------------------

/**
 * Returns a list of user IDs matching the specific criteria
 *
 * @param array $ids
 * An array of user IDs or role IDs, or an empty array
 *
 * @param type $type
 * Which type of array is being sent.  Possible values:
 *  - by_role
 *  - by_user
 *  - all_users
 *
 * @param boolean $only_new
 * If TRUE, this function returns ids only for users who have not yet logged in.
 *
 * @return array
 */
function _loginlink_get_userlist($ids = array(), $type = 'by_user', $only_new = FALSE) {

  //Get a list of user ids
  if ($type == 'by_role') {

    $uids = array();

    $or_conditions = db_or();
    foreach ($ids as $id) {

      //If 'Authenticated User Role, special logic
      if ($id == user_role_load_by_name('authenticated user')->rid) {

        $q = db_select('users', 'u');
        $q->leftJoin('users_roles', 'ur', 'ur.uid = u.uid');
        $q->fields('u', array('uid'));
        $q->fields('ur', array('rid'));
        $q->condition('ur.rid', NULL);
        $qr = $q->execute();

        foreach ($qr as $r) {
          $uids[] = $r->uid;
        }
      }

      //Otherwise just run it as normal
      $or_conditions->condition('rid', $id);
    }

    $q = db_select('users_roles', 'ur')->fields('ur', array('uid'))->condition($or_conditions)->execute();

    foreach ($q as $r) {
      $uids[] = $r->uid;
    }
  }

  elseif ($type == 'by_user') {
    $uids = $ids;
  }

  elseif ($type == 'all_users') {

    //Get the users
    $q = db_select('users', 'u')->fields('u', array('uid', 'name'))->condition('status', '1')->execute();
    $uids = array();
    foreach ($q as $r) {
      $uids[] = $r->uid;
    }

  }

  //@TODO: What about blocked users?

  //Weed out duplicates
  $uids = array_unique($uids);

  //Weed out the '0' user
  sort($uids);
  if (count($uids) > 0 && $uids[0] == '0')
    array_shift($uids);
  
  //If only_new, weed out anybody who has logged-in
  if (count($uids) > 0 && $only_new) {

    //Get users who have nevery logged in
    $q = db_select('users', 'u')->fields('u', array('uid'))->condition('login', '0')->execute();
    $new_uids = array();
    foreach ($q as $r) {
      $new_uids[] = $r->uid;
    }

    //Return all those values that are in the uids and are new
    $uids = array_values(array_intersect($uids, $new_uids));
  }

  //Return the list of ids
  return $uids;
}

//------------------------------------------------------------------------

/**
 * CSV Prep
 *
 * Helper function to encode a string for a CSV file
 *
 * @param string $str
 * @return string
 */
function csv_prep($str) {
  return '"' . str_replace('"', '""', $str) . '"';
}

/* EOF: login_link_admin.inc */