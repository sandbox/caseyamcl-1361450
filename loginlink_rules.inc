<?php

/**
 * @file
 * Loginlink Rules Module Integration
 */

//------------------------------------------------------------------------

/**
 * Implements hook_rules_event_info().
 *
 * @ingroup rules
 */

function loginlink_rules_rules_event_info() {
  return array(
    'loginlink_used' => array(
      'label' => t('When the user has logged in via a loginlink'),
      'group' => t('Loginlink'),
      'variables' => array(
        'account' => array(
          'type' => 'user',
          'label' => t("The user's account"),
        ),
      ),
    ),
  );
}

/* EOF: loginlink_rules.inc */